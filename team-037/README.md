# EDOM - Projecto de Tópico de Pesquisa

O presente documento foi desenvolvido no âmbito da unidade curricular de Engenharia de Domínio (EDOM), no primeiro ano do Mestrado em Engenharia Informática (MEI), no ramo de Engenharia de Software, no Instituto Superior de Engenharia do Porto (ISEP).

O projeto foi desenvolvido por: Filipe Oliveira - 1160826, Pedro Ferreira - 1140953, Vitor Melo - 1161258

---

# Cloudfier - _"A platform for rapid deployment of business apps"_

## Introdução

Cloudfier é uma ferramenta _open-source_ para o desenvolvimento de aplicações de negócio com base em modelos executáveis. 

Normalmente utilizada para protótipos, _Cloudfier_ usa transformações _Model-To-Text_ utilizando linguagem _UML_ via _TextUML_ permitindo a criação de softwares _mockup_ rapidamente. 
O facto de utilizar _TextUML_ diminui a ‘barreira’ que existe entre a visão do produto final do cliente e o esforço do _developer_, contribuindo para um produto final ‘saudável’ que não sofreu alterações profundas na sua constituição a meio da fase de desenvolvimento. 

De referir que a utilização de _TextUML_ diminui também a dependência de uma tecnologia especifica, ou seja, não é necessário conhecimento extra sobre uma determinada tecnologia para desenvolver produto final. 

---

## IDE -> Eclipse Orion

Eclipse Orion é um IDE _open-source_ que permite desenvolvimento de _sofware_ exclusivamente na _cloud_ sem ser necessário a instalação de qualquer software na maquina local. 

Isto permite uma liberdade enorme tanto que é possivel desenvolver praticamente em qualquer _device_ desde que este contenha um _browser_.

Este IDE foi escolhido para ser o IDE pré definido e o mais recomendado para desenvolvimento de aplicações Cloudfier, o que aumenta ainda mais a liberdade e a facilidade de desenvolver nesta plataforma.

De referir que este IDE ainda apresenta ferramentas compativeis com repositorios _Git_ e uma _Shell_ integrada.

![orion](img/7.png)


---

## Base de uma aplicação Cloudifier

Um projeto cloudifier tem como base um ficheiro de propriedades chamado _mdd.properties_ que é obrigatório em todos os projetos e criado automaticamente e Entidades. Este ultimo é o _core_ do negócio e do desenvolvimento sendo que a API resultante e a UI, serão baseadas nestas entidades bem como as suas propriedades, acções, relações com outras entidades e etc.

![mdd.properties](img/1.png)

## Conceitos

Os conceitos base dos ficheiros .tuml contido em cada um dos projetos passam por ser os seguintes:

>Entidades -> Correspondem às abstrações primárias numa aplicação de gestão de dados. Cliente, Despesa, Conta e Compra são exemplos de entidades de vários domínios.

>Propriedades -> São bocados de informação que descrevem as entidades. As propriedades são _typed_ e, como tal, aceitam apenas valores compatíveis com seus tipos. As propriedades podem ser obrigatórias ou opcionais.
Uma propriedade pode ser derivada e, como tal, seu valor não pode ser modificado diretamente, mas apenas modificando os valores originais dos quais deriva (se esta não for uma constante).

![entity](img/2.png)

>Relações -> Relações são associações entre entidades. Podem ser obrigatórias, opcionais, únicas ou múltiplas. As relações também podem ser uni ou bidirecionais. Compra x item, item x produto, conta x proprietário, reunião x participante são exemplos de relações entre diferentes entidades.

![relation](img/3.png)

>Ações -> Ações representam mudanças que podem ser aplicadas a instâncias de entidades. As ações podem conter parâmetros, que podem ser preenchidos no momento em que a ação é executada. Ações são geralmente verbos. Exemplos de ações são: _checkout_ (pedido de compra), retirar (da conta) e marcar (reunião).
As ações podem especificar pré-condições para que sejam aplicáveis ​​apenas se a instância as cumprir. Por exemplo, uma compra só pode ser realizada se tiver pelo menos um item, uma compra só pode finalizar se o utilizador ter fundos suficientes, etc.

![action](img/4.png)

>Consultas _'Queries'_ -> As _queries_ permitem a listagem de objetos que cumpram determinados critérios. As _queries_ geralmente são parametrizadas. Um exemplo é uma _query_ que mostra todos os Problemas com um nivel de Urgência especifico.

![query](img/5.png)

>Restrições -> As restrições especificam restrições impostas pelas regras de negócios de forma declarativa. Podem ser invariantes de classe e atributo (sempre devem ser cumpridas) e pré-condições (devem ser cumpridas antes de executar uma certa ação).

>Máquinas de estado _'state machines'_ -> Máquinas de estado representam os estados que uma entidade pode conter, as possíveis transições entre esses estados e as condições que estes devem ocorrer.
Por exemplo, os estados de uma encomenda podem ser "Em progresso", "Submetido", "Enviado" e "Entregue".

![state](img/6.png)

>Serviços -> Os serviços fornecem funcionalidades exigidas pela aplicação, mas geralmente são realizadas externamente. Exemplos são _UserNotificationService_, _PaymentService_ e _StockQuoteService_. Os serviços podem fornecer serviços de consulta de dados síncronos (via _queries_ e ações) e comunicação assíncrona (via sinais).

>Sinais _'Signals'_ -> Os sinais permitem que uma aplicação publique ou receba eventos. Geralmente são utilizados de forma a transmitir informação entre aplicações diferentes.

---

## Desenvolvimento

O desenvolvimento de aplicações Cloudfier, como dito anteriormente, passa pela criação de um ficheiro de propriedades, o ficheiro (ou ficheiros) de entidades escritos em TextUML que usufruem dos conceitos descritos e opcionalmente um ficheiro data.JSON contendo dados para popular a base de dados aquando o _Deployment_ (_seed_).

Cloudfier segue uma abordagem muito _straight foward_ sendo que, o que é implementado é o que é mostrado aquando o _deployment_.

![editor](img/8.png)

---

## _Deployment_

O _deployment_ é efectuado de uma maneira simples utilizando a _shell_ imbutida no Orion. 

Utilizando o comando _'cloudfier full-deploy'_ equivale a fazer _deploy_ da aplicação desenvolvida e da base de dados com o _schema_ correspondente ao TextUML desenvolvido. Se existir um ficheiro de _seed_ de base de dados (data.json) este será utilizado para popular a base de dados.

![deploy](img/9.png)


---

## Resultado Final

Depois do _full deploy_, ou seja, a 'transformação' do modelo UML para código e por consequinte, para a aplicação em si, ficamos com uma estrutura que corresponde a:

- _Simple UI_ -> Uma _User Interface_ simples contendo um sistema de autenticação por predefinição, vistas/paginas que permitem o CRUD das entidades estabelecidas e outras funcionalidades.
- _REST API_ -> Uma _API REST_ contendo a documentação autogerada, serviços disponiveis bem como as _queries_ e as instâncias das entidades.
- _Business Layer_ -> Camada de negocio da API contendo todas as regras de negocio/restrições/pré-condições estabelecidas no .tuml anterior.
- _Database_ -> Uma base de dados pronta a ser utilizada contendo , ou não, dados pré-definidos. Atualmente está limitada ao PostgreSQL.


![deploy](img/10.png)


![deploy](img/11.png)

---

## Conclusão

Podemos concluir que Cloudfier é uma boa ferramenta para utilizar em prototipos iniciais aquando a fase de planeamento e decisão de um projeto.

Apesar de uma base solida e bastantes funcionalidades, não é de todo recomendado utilizar em ambiente de Produção dado a sua limitação em vários aspectos só executáveis em frameworks mais especificas.

Contudo, após analise breve por parte do grupo, o Cloudifier pode ser util na rápida criação de microserviços (REST API) e manutenção destes mesmos. Para isso, esta solução precisa de uma forte compatibilidade com o ambiente devops em voga nos dias de hoje e aumentar a sua estabilidade bem como a compatibilidade.