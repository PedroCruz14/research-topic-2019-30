# Epsilon Transformation Language (ETL) #

O suporte à automatização de transformação de modelos é fundamental para a realização do processo 
MDD (Model Driven Development). É essencial aplicar o MDD no amplo suporte automatizado para várias tarefas adicionais, 
como comparação de modelos, merging, validação e transformação de modelo em texto.
Embora várias linguagens de transformação de modelo bem-sucedidas tenham sido propostas atualmente,
a maioria delas foi desenvolvida isoladamente e, como resultado, enfrentam dificuldades de consistência
e integração com linguagens que suportam outras tarefas de gestão de modelo. Aqui iremos apresentar a 
Epsilon Transformation Language (ETL) que é uma linguagem de transformação de modelo híbrida que foi desenvolvida
sobre a infraestrutura fornecida pela plataforma de gestão de modelos Epsilon. Com a criação do Epsilon,
o ETL é perfeitamente integrado a várias outras linguagens específicas de tarefas para ajudar a realizar
fluxos de trabalho de geestãoo de modelos compostos.

## Características ##

- Transformação de vários modelos de entrada(inputs) em vários modelos de saída(output)
- Capacidade de consultar, navegar e modificar os modelos de origem e destino
- Execução automatizada de regras
- Regras "lazy" e "greedy"
- Herança de várias regras
- Suporte para acedder/modificar simultaneamente muitos modelos de (potencialmente) diferentes metamodelos
- Todas as estruturas usuais de programação (while e for loops, sequência de instruções, variáveis etc.)
- Suporte para operações convenientes de lógica OCL (selecionar, rejeitar, coletar etc.)
- Capacidade de criar e chamar métodos de objetos Java
- Suporte para interação do utilizador
- Capacidade de criar bibliotecas reutilizáveis de operações e importá-las de diferentes módulos Epsilon

## Exemplo ##

Será a seguir apresentado um exemplo de transformação modelo-modelo através do ETL.

### Tree.emf ###

@namespace(uri="Tree", prefix="Tree")

package Tree;

class Tree {

   val Tree[*]#parent children;
   
   ref Tree#children parent;
   
   attr String label;
   
}



### Tree2Graph.etl ###


rule Tree2Node 

  transform t : Tree!Tree
  
  to n : Graph!Node {
  
  n.name = t.label;
  
  // If t is not the top tree
  
  // create an edge connecting n
  
  // with the Node created from t's parent
  
  if (t.parent.isDefined()){
  
    var e : new Graph!Edge;
	
    e.source ::= t.parent;
	
    e.target = n;
	
  }  
}


### Graph.emf ###


@namespace(uri="Graph", prefix="Graph")

package Graph;

class Graph {

   val Node[*] nodes;
   
}

class Node {

   attr String name;
   
   val Edge[*]#source outgoing;
   
   ref Edge[*]#target incoming;
   
}

class Edge {

   ref Node#outgoing source;
   
   ref Node#incoming target;
   
}

## Alternativas ##

ATL

ATL é uma linguagem de transformação de modelos e fornece várias ferramentas nesse sentido.
O ATL disponibiliza formas de produzir um conjunto de modelos destino a partir de unconjunto de modelos origem.
Esta linguagem foi desenvolvida sobre a plataforma Eclipse onde são fornecidos vários padrões e ferramentas que visam
facilitar o desenvolvimento de transformações ATL.
A principal diferença entre ATL e ETL é que ETL é uma linguagem mais imperativa sendo que o aTL oferece algumas mlimktações neste sentido.

## Conclusão ##

Resumidamente o ETL é uma linguagem de transformação de modelo híbrida que se diferencia das outras por ser facilmente
integrado com o resto das ferramentas da plataforma Epsilon.

## Bibliografia ##

https://www.eclipse.org/epsilon/doc/etl/

Kolovos D.S., Paige R.F., Polack F.A.C. (2008) The Epsilon Transformation Language. In: Vallecillo A., Gray J., Pierantonio A. (eds) Theory and Practice of Model Transformations. ICMT 2008. Lecture Notes in Computer Science, vol 5063. Springer, Berlin, Heidelberg

**Voltar para o readme.md principal**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](README.md)
