# EOL #

O Eclipse Object Language (EOL) é a linguagem central de todas as outras lisnguagens disponibilizadas pelo Epsilon. Todas elas (EVL, ETL e EGL) são contruídas por cima do EOL.

![eol-diagram](img/eol-diagram.png)

É uma linguagem que permite a criação, consulta e modificação de modelos EMF. Providencia funcionalidades encontradas no Javascript (e.g. variáveis, loops for e while, ifs, etc.), assim como funcionalidades do OCL (e.g. funções de consulta de coleção).

## Características ##

* Acesso e modificação a vários modelos/metamodelos em simultâneo;
* Todos os construtores usuais de programação;
* Operações  OCL de primeira ordem (i.e. select, reject, collect, etc.);
* Criação e chamada de métodos de objectos Java. **Exemplo**:

Instanciação de um JFrame em EOL

```eol
var jframe := new Native('javax.swing.JFrame');
jframe.setBounds(0,0,100,100);
-- Equivalent to jframe.setTitle('Opened from EOL');
jframe.title := 'Opened from EOL';
jframe.visible := true;
```

* Suporte para operações em cache (as operações em cache são apenas executadas uma vez e sempre que forem chamadas retornam o mesmo resultado);
* Suporte para propriedades extendidas. **Exemplo**:

Dado o seguinte metamodelo (em Emfatic):

```emfatic
package SimpleTree;

class Tree {
  attr String name;
  ref Tree#children parent;
  val Tree[*]#parent children;
}
```

Pretende-se verificar qual é a profundudade de cada árvore num modelo em conformidade com o metadelo exemplo e fazer um print dessa profundidade. Uma hipótese seria adicionar o atributo *depth* ao metamodelo, no entando essa hipótese é descartada porque se estaria a a colocar informação de importância secundário ao metamodelo. Tendo isto em conta existem duas hipótese: utilizar um *depth Map* para o programa EOL e ou adicionar uma *extended property* ao metamodelo.

Programa EOL **sem** *extended properties*:

```eol
var depths = new Map;

for (n in Tree.allInstances.select(t|not t.parent.isDefined())) {
  n.setDepth(0);
}

for (n in Tree.allInstances) {
  (n.name + " " + depths.get(n)).println();
}

operation Tree setDepth(depth : Integer) {
  depths.put(self,depth);
  for (c in self.children) {
    c.setDepth(depth + 1);
  }
}
```

Programa EOL **com** *extended properties*:

```eol
for (n in Tree.allInstances.select(t|not t.parent.isDefined())) {
  n.setDepth(0);
}

for (n in Tree.allInstances) {
  (n.name + " " + n.~depth).println();
}

operation Tree setDepth(depth : Integer) {
  self.~depth = depth;
  for (c in self.children) {
    c.setDepth(depth + 1);
  }
}
```

A propriedade extendida é inicializada com um "~". Quando um valor é atribuido a uma propriedade destas, a propriedade é criada e associada ao objecto atual. Esta abordagem é útil visto que é possível anexar informação útil (não suportada pelo metamodelo) a elementos de um modelo em particular durante a operações de **validação**, **tranformação** e **geração de texto**);

* Suporte para interação com o utilizador;
* Abilidade de criação de bibliotecas reutilizáveis, permitindo também a sua importação para diferentes módulos do Epsilon (não só EOL);
* Monitorização de programas do Epsilon: é possível obter métricas da performance de código do Epsilon, identificando blocos de código executados mais vezes do que o esperado ou que demorem demasiado tendo na sua execução. Para tal, o profiler deve ser adicionado ao código da seguinte forma:

```eol
var profiler : new Native('org.eclipse.epsilon.eol.tools.ProfilerTool');
profiler.start('Program');

// ...
// Bloco de código a ser monitorizado
// ...

profiler.stop();
```

Posteriormente, para visualisar a informação do profiler, deve-se selectionar a view **Profiling**.

Fontes:

* [Apresentação do Dimitris Kolovos (Epsilon lead developer)](https://www.slideshare.net/dskolovos/epsilon-22064299)
* [Documentação do EOL](https://www.eclipse.org/epsilon/doc/eol/)

**Voltar para o readme.md principal**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](README.md)
