# EDOM - Projecto de Tópico de Pesquisa

O presente documento foi desenvolvido no âmbito da unidade curricular de Engenharia de Domínio (EDOM),
no primeiro ano do Mestrado em Engenharia Informática (MEI), no ramo de Engenharia de Software, no Instituto Superior de Engenharia do Porto (ISEP).

O projeto foi desenvolvido por: 

- José Marinho - 1161522
- Francisco Bobiano - 1160593
- Ricardo Quintas - 1161235
- Tiago Teixeira - 1160581

# EMFText

O EMFText é um plug-in do Eclipse que permite definir a sintaxe do texto para as linguagens descritas por um metamodelo Ecore. O EMFText permite que os _developers_ definam linguagens específicas do domínio textual rapidamente e sem a necessidade de aprender novas tecnologias e conceitos, e gera ferramentas de suporte para essas linguagens.

![alt text](EMFText.png "EMFText")

*Figura 1 - EMFText*

## Como usar
O processo de desenvolvimento de uma linguagem com EMFText é apresentado na seguinte figura.

![alt text](DesenvolvimentoEMFText.png "Desenvolvimento EMFText")

*Figura 2 - Processo de desenvolvimento de uma linguagem com EMFText*

#### Especificar a linguagem do metamodelo

Elementos de um metamodelo:

- Classes, _data types_, enumeradores, atributos, referências, cardinalidades, herança.

Criando um metamodelo:

- Definir conceitos, relações e propriedades num modelo _Ecore_;
- Metamodelos existentes podem ser importados.

#### Especificar uma _syntax_ concreta

- Representação textual de todos os conceitos do metamodelo;
- Automaticamente cria o gerador de _syntax_;
- É possível especificar manualmente a _syntax_ em concreto;
- As regras específicas da _syntax_ são derivadas da linguagem específica da _syntaxt_ do EBNF (família de notações meta-sintaxe).

Uma especificação de sintaxe EMFText deve estar contida num arquivo com a extensão .cs e consiste em quatro blocos principais:

1. Um bloco de configuração obrigatório, que especifica o nome da sintaxe (ou seja, o arquivo extensão), o modelo do gerador onde encontrar as metaclasses e a metaclasse raiz (símbolo de início). Opcionalmente, outras sintaxes e metamodelos podem ser importados e codificar opções de geração podem ser especificadas.

2. Uma seção (opcional) TOKENS. Aqui, tipos de token como identificadores, números etc. para o analisador lexical pode ser especificado.

3. Uma seção (opcional) de TOKENSTYLES. Aqui, o estilo padrão (ou seja, cor e estilo da fonte) para tokens e palavras-chave podem ser especificados.

4. Uma seção RULES, que define a sintaxe para metaclasses de concreto.

![alt text](Blocos.png "Blocos principais")

*Figura 3 - Blocos principais*

## Model Driven Engineering (MDE)

O **_Model Driven Engineering_** (**MDE**) é uma abordagem de desenvolvimento de software que considera o uso sistemático de modelos para desenvolver software em vez de usar linguagens de programação de uso geral.

As vantagens de usar MDE:

- Aumentos significativos de produtividade (cinco a dez vez mais rápido do que os métodos comummente utilizados);
- Facilidade de manutenção;
- Interoperabilidade entre os vários departamentos que estejam a trabalhar no mesmo projeto;
- Reutilização de software que estiver em uso;
- Aumenta a qualidade tanto do produto final como dos processos.

## Cenário - Aplicação de Gestão de Contas Bancárias

Um banco multinacional necessita de uma aplicação capaz de gerir a conta bancária de todos os seus clientes. A	aplicação será desenvolvida por uma equipa de informáticos e implementada para todos os bancos a nível nacional e internacional.
Tendo por base o _Model Driven Engineering_ (MDE):

- Será elaborado um metamodelo conforme os requisitos do cliente;
- As DSL's serão geradas usando a ferramenta EMFText;
- As _Model to Model Transformations_ serão feitas com recurso ao ATL;
- As _Model to/from Text Transformations_ serão feitas usando o _Aceeleo_.

Após a criação do projeto do tipo EMFText, foi definido o metamodelo apresentado na Figura 4.

![alt text](metamodelo.PNG "Metamodelo")

*Figura 4 - Metamodelo*

Depois foi então gerada a DSL utilizando a ferramenta EMFText.

![alt text](generateDsl.png "Gerando a DSL")

*Figura 5 - Gerando a DSL*

![alt text](dsl.PNG "DSL")

*Figura 6 - DSL*

## Referências

- https://github.com/DevBoost/EMFText
- https://pt.slideshare.net/eclipsedayindia/dsl-and-emttext
- https://www.researchgate.net/publication/320756996_Software_Development_Tools_in_Model-Driven_Engineering