#region Using directives

using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;

#endregion

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle(@"")]
[assembly: AssemblyDescription(@"")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(@"Group11")]
[assembly: AssemblyProduct(@"GourgeousFood")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: System.Resources.NeutralResourcesLanguage("en")]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion(@"1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
[assembly: ReliabilityContract(Consistency.MayCorruptProcess, Cer.None)]

//
// Make the Dsl project internally visible to the DslPackage assembly
//
[assembly: InternalsVisibleTo(@"Company.GourgeousFood.DslPackage, PublicKey=0024000004800000940000000602000000240000525341310004000001000100E940E47AF99EAD45AAC39E37CBB83984904D39FBF5FE60A6D7C5E1808C7D64921827C7484BB15FE1921C0717245420FBCA8C8525E0076065AC2658682E0E030CCD5F029284DC2A3630A63A6B2950FA22B1A38671D63F97FC2161DEDAB7752F2110CABB24233D7AAA16B8ADC60AA8BC67846BC85049258712D87755C5327491BC")]