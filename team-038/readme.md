# EDOM - Projecto de Tópico de Pesquisa

O presente documento foi desenvolvido no âmbito da unidade curricular de Engenharia de Domínio (EDOM), no primeiro ano do Mestrado em Engenharia Informática (MEI), no ramo de Engenharia de Software, no Instituto Superior de Engenharia do Porto (ISEP).

O projeto foi desenvolvido por:
* António Nascimento - 1130573
* Pedro Cruz - 1160988

---

# Graphiti

O _Eclipse_ fornece uma infraestrutura de modelação desenvolvida em torno do _Eclipse Modeling Framework (EMF)_, para o qual é essencial oferecer representações gráficas e possibilidades de edição.

_**Graphiti**_ é uma _framework_ gráfica baseada no _Eclipse_ que permite o rápido desenvolvimento de editores de diagrama, para modelos de domínio, de ultima geração. A ferramenta tanto pode usar modelos de domínio baseados em _EMF_ fácilmente, como pode trabalhar em torno de objetos de _Java_ no que diz respeito ao domínio.

Os objetivos descritos para o _**Graphiti**_ são: 

1. Fornecer uma _API_ Java simples, de fácil utilização e bem estruturada para a construção de ferramentas gráficas 
2. Fornecer documentação e tutoriais para fazer isso
3. Diminuir as dependências da _framework_ o máximo possivel para que possa suportar cenários do tipo _RCP_
4. Fornecer componentes opcionais - além do caso de uso do _RCP_ - para facilitar, por exemplo integração no _IDE_
5. Fornecer a capacidade de usar qualquer algoritmo de _layout_ existente para o _layout_ automático de um diagrama

----

# Exemplo do Plug-In

Dado um metamodelo, esta framework permite a crialção de em editor para criar uma representação gráfica do mesmo.

![MetaModel](./FileSystemModel.png)

O exemplo acima apresentado é o metamodelo de um sistema de ficheiros muito simples em que se podem criar vários _Filesystems_ e cada um desses pode estar ligado a um conjunto de _files_ e de _folders_. Por sua vez, cada folder poderá também ter uma referência a mais _folders_ e/ou _files_.

Após a criação de um plug-in e da sua devida configuração é possível a criação de um editor gráfico como apresentado na figura abaixo.

![MetaModel](./FileSystemEditor.png)

---

# Configuração do Plug-In

Cria-se um projeto _plug-in_ para o tipo de diagrama específico à ferramenta gráfica a ser desenvolvida.

Inicialmente, cria-se um _Diagram Type Provider_ como demonstrado no excerto de código que se segue:
```java
package org.eclipse.graphiti.filesystem.diagram;

public class FileSystemDiagramTypeProvider extends AbstractDiagramTypeProvider {

        public FileSystemDiagramTypeProvider() {
            super();
            setFeatureProvider(new FileSystemFeatureProvider(this));
        }
}
```

        setFeatureProvider(...) -> serve para adicionar features ao nosso provider

Após a criação do _Diagram Type Provider_ e o _Feature Type Provider_ provider estes devem ser registados no _**pom.xml**_ como demonstrado no excero de xml:

```xml
<plugin>
  <extension
      point="org.eclipse.graphiti.ui.diagramTypes">
    <diagramType
      description="This is the diagram type for my filesystem"
      id="org.eclipse.graphiti.filesystem.diagram.FileSystemDiagramType"
      name="My Filesystem Diagram Type"
      type="myfilesystem">
    </diagramType>
  </extension>

  <extension
      point="org.eclipse.graphiti.ui.diagramTypeProviders">
    <diagramTypeProvider
      class="org.eclipse.graphiti.filesystem.diagram.FileSystemDiagramType"
      description="This is my editor for my filesystem"
      id="org.eclipse.graphiti.examples.tutorial.diagram.FileSystemDiagramTypeProvider"
      name="My filesystem editor">
      <diagramType
        id="org.eclipse.graphiti.filesystem.diagram.MyTutorialDiagramType">
      </diagramType>
    </diagramTypeProvider>
  </extension>
</plugin>
```

O _FileSystemFeatureProvider_ será a classe onde se definem as funcionalidades oferecidas pelo editor gráfico a ser desenvolvido.

Como se pode verificar pelo excerto abaixo, a ferramenta a ser desenvolvida oferece a possibilidade de acidionar um _File_ e adicionar um _Containment_ (associação entre o _filesystem_ e os _files_).

```java
package org.eclipse.graphiti.filesystem.diagram;

public class FileSystemFeatureProvider extends DefaultFeatureProvider {
 
    public FileSystemFeatureProvider(IDiagramTypeProvider dtp) {
        super(dtp);
    }

    @Override
    public IAddFeature getAddFeature(IAddContext context) {
        // is object for add request a Flie?
        if (context.getNewObject() instanceof File) {
            return new TutorialAddFileFeature(this);
        } else if (context.getNewObject() instanceof Containment) {
            return new TutorialAddContainmentFeature(this);
        }
        return super.getAddFeature(context);
    }
}  
```

Para o caso do _AddFile_, este extende a _AbstractAddShapeFeature_ uma vez que se trata de um elemento do metamodelo (não é uma ligação). No adicionar, configura-se o _look_ que deverá ter aquando da adição à UI.

É necessário implementar também o _update_ para que as alterações sejam visíveis no editor.

As _features_ de remover/apagar não são necessárias implementar uma vez que as que v
em por _default_ são o suficiente para a boa utilização da ferramenta. No entanto, é sempre possível dar override às mesmas, caso se queira.

**AddFileFeature**
```java
package org.eclipse.graphiti.filesystem.features;
 
public class TutorialAddFileFeature extends AbstractAddShapeFeature {
 
    private static final IColorConstant E_CLASS_TEXT_FOREGROUND = IColorConstant.BLACK; 
    private static final IColorConstant E_CLASS_FOREGROUND = new ColorConstant(98, 131, 167);
    private static final IColorConstant E_CLASS_BACKGROUND = new ColorConstant(187, 218, 247);
 
    public TutorialAddFileFeature(IFeatureProvider fp) {
        super(fp);
    }
 
    public boolean canAdd(IAddContext context) {
        // check if user wants to add a File
        if (context.getNewObject() instanceof File) {
            // check if user wants to add to a diagram
            if (context.getTargetContainer() instanceof Diagram) {
                return true;
            }
        }
        return false;
    }
 
    public PictogramElement add(IAddContext context) {
        File addedClass = (File) context.getNewObject();
        Diagram targetDiagram = (Diagram) context.getTargetContainer();
 
        // CONTAINER SHAPE WITH ROUNDED RECTANGLE
        IPeCreateService peCreateService = Graphiti.getPeCreateService();
        ContainerShape containerShape = peCreateService.createContainerShape(targetDiagram, true);
 
        // define a default size for the shape
        int width = 100;
        int height = 50;
        IGaService gaService = Graphiti.getGaService();
        RoundedRectangle roundedRectangle; // need to access it later
 
        {
            // create and set graphics algorithm
            roundedRectangle = gaService.createRoundedRectangle(containerShape, 5, 5);
            roundedRectangle.setForeground(manageColor(E_CLASS_FOREGROUND));
            roundedRectangle.setBackground(manageColor(E_CLASS_BACKGROUND));
            roundedRectangle.setLineWidth(2);

            gaService.setLocationAndSize(roundedRectangle, context.getX(), context.getY(), width, height);
 
            // if added Class has no resource we add it to the resource of the diagram
            // in a real scenario the business model would have its own resource
            if (addedClass.eResource() == null) {
                     getDiagram().eResource().getContents().add(addedClass);
            }
            // create link and wire it
            link(containerShape, addedClass);
        }
 
        // SHAPE WITH LINE
        {
            // create shape for line
            Shape shape = peCreateService.createShape(containerShape, false);
 
            // create and set graphics algorithm
            Polyline polyline = gaService.createPolyline(shape, new int[] { 0, 20, width, 20 });
            polyline.setForeground(manageColor(E_CLASS_FOREGROUND));
            polyline.setLineWidth(2);
        }
 
        // SHAPE WITH TEXT
        {
            // create shape for text
            Shape shape = peCreateService.createShape(containerShape, false);
 
            // create and set text graphics algorithm
            Text text = gaService.createText(shape, addedClass.getName());
            text.setForeground(manageColor(E_CLASS_TEXT_FOREGROUND));
            text.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER );
            // vertical alignment has as default value "center"
            text.setFont(gaService.manageDefaultFont(getDiagram(), false, true));
            gaService.setLocationAndSize(text, 0, 0, width, 20);
 
            // create link and wire it
            link(shape, addedClass);
        }
 
        return containerShape;
    }
}
```

**AddContainmentFeature**

```java
package org.eclipse.graphiti.filesystem.features;
 
public class TutorialAddContainmentFeature extends AbstractAddFeature {

    private static final IColorConstant E_REFERENCE_FOREGROUND = new ColorConstant(98, 131, 167);
 
    public TutorialAddContainmentFeature (IFeatureProvider fp) {
        super(fp);
    }
 
    public PictogramElement add(IAddContext context) {
        IAddConnectionContext addConContext = (IAddConnectionContext) context;
        Containment addedContainment = (Containment) context.getNewObject();
        IPeCreateService peCreateService = Graphiti.getPeCreateService();
       
        // CONNECTION WITH POLYLINE
        Connection connection = peCreateService
            .createFreeFormConnection(getDiagram());
        connection.setStart(addConContext.getSourceAnchor());
        connection.setEnd(addConContext.getTargetAnchor());
 
        IGaService gaService = Graphiti.getGaService();
        Polyline polyline = gaService.createPolyline(connection);
        polyline.setLineWidth(2);
        polyline.setForeground(manageColor(E_REFERENCE_FOREGROUND));
 
        // create link and wire it
        link(connection, addedContainment);
 
        return connection;
    }
 
    public boolean canAdd(IAddContext context) {
        // return true if given business object is an Containment
        // note, that the context must be an instance of IAddConnectionContext
        if (context instanceof IAddConnectionContext
            && context.getNewObject() instanceof Containment) {
            return true;
        }
        return false;
    }
}
```
---

**Adicionalmente**

A nível gráfico são possíveis configurar as várias formas, tamanhos, cores, entre outras. Sendo também possível adicionar imagens ao invés das formas mais simpeles caso se queira.